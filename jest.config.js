module.exports = {
  'verbose': true,
  'expand': false,
  'globals': {
    'NODE_ENV': 'test'
  },
  'moduleFileExtensions': [
    'js',
    'jsx',
    'ts',
    'tsx'
  ],
  'transform': {
    '^.+\\.tsx?$': 'ts-jest'
  },
  'testMatch': [
    '**/__tests__/**/?(*.)+(spec|test).ts?(x)',
    '**/?(*.)+(spec|test).js?(x)'
  ],
  'moduleDirectories': [
    'node_modules',
    'src'
  ],
  'transformIgnorePatterns': []
}