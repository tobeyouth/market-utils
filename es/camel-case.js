export default (function (name, separator) {
    if (separator === void 0) { separator = '-'; }
    var firstPattern = /^\S/gi;
    var camelPattern = new RegExp("\\" + separator + "([a-z]{1})", 'gi');
    return name.replace(firstPattern, function ($1) {
        return $1.toLowerCase();
    }).replace(camelPattern, function ($1, $2) {
        return $2.toUpperCase();
    });
});
