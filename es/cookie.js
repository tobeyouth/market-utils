var DEFALT_DOMAIN = 'douban.com';
var DEFALT_PATH = '/';
export default {
    set: function (dict, days, domain, path) {
        if (days === void 0) { days = 1; }
        if (domain === void 0) { domain = DEFALT_DOMAIN; }
        if (path === void 0) { path = DEFALT_PATH; }
        var date = new Date();
        var expires;
        date.setTime(date.getTime() + ((days || 30) * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toUTCString();
        try {
            for (var k in dict) {
                if (dict.hasOwnProperty(k)) {
                    document.cookie = k + "=" + dict[k] + expires + ";domain=" + domain + ";path=" + path;
                }
            }
        }
        catch (_a) {
            return false;
        }
        return true;
    },
    get: function (name) {
        var n = name + "=";
        var ca = document.cookie.split(';');
        var i;
        var c;
        for (i = 0; i < ca.length; i++) {
            c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1, c.length);
            }
            if (c.indexOf(n) === 0) {
                return c.substring(n.length, c.length).replace(/\"/g, '');
            }
        }
        return '';
    }
};
