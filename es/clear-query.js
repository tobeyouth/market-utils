/**
 * 清除 query 中为空或者未定义的 param
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
// helper
function isCanClear(val) {
    return val === '' || val === undefined ||
        val === null || (typeof (val) === 'number' && isNaN(val)) ||
        (Array.isArray(val) && val.length === 0);
}
export default (function (query) {
    var _query = __assign({}, query);
    for (var k in _query) {
        if (_query.hasOwnProperty(k) && isCanClear(_query[k])) {
            delete _query[k];
        }
    }
    return _query;
});
