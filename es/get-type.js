/**
 * return param type
 */
export default (function (param) {
    var reg = /^\[object ([a-z]+)\]$/gi;
    var r = Object.prototype.toString.call(param)
        .replace((reg), function ($1, $2) { return $2; });
    if (r) {
        return r.toLocaleLowerCase();
    }
    return '';
});
