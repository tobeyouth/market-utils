export var DEFAULT = 'default';
export var PENDING = 'pending';
export var FAIL = 'fail';
export var SUCCESS = 'success';
export var ASYNC_STATUS = [DEFAULT, PENDING, FAIL, SUCCESS];
