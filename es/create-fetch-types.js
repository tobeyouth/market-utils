/**
 * 创建带 scope 和 status 的 fetch action types
 * example:
 * createFetchTypes('topics', 'fetch')
 * return:
 * {
 *  fetch: 'TOPICS.FETCH',
 *  success: 'TOPICS.FETCH.SUCCESS',
 *  fail: 'TOPICS.FETCH.FAIL'
 * }
 */
import { SUCCESS, FAIL, PENDING } from './async-status';
export default (function (scope, name) {
    var _a;
    if (!scope) {
        throw new Error('定义 action types 时缺少 scope');
    }
    var _default = [scope, name].map(function (item) { return item.toUpperCase(); }).join('.');
    var _success = [scope, name, SUCCESS].map(function (item) { return item.toUpperCase(); }).join('.');
    var _pending = [scope, name, PENDING].map(function (item) { return item.toUpperCase(); }).join('.');
    var _fail = [scope, name, FAIL].map(function (item) { return item.toUpperCase(); }).join('.');
    return _a = {},
        _a[name] = _default,
        _a.pending = _pending,
        _a.success = _success,
        _a.fail = _fail,
        _a;
});
