var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import 'whatwg-fetch';
import json2string from './json2string';
import { urlWithQuery, compileUrl } from './url';
import getType from './get-type';
export var SUPPORT_METHODS;
(function (SUPPORT_METHODS) {
    SUPPORT_METHODS["get"] = "GET";
    SUPPORT_METHODS["post"] = "POST";
    SUPPORT_METHODS["put"] = "PUT";
    SUPPORT_METHODS["delete"] = "DELETE";
    SUPPORT_METHODS["options"] = "OPTIONS";
    SUPPORT_METHODS["patch"] = "PATCH";
    SUPPORT_METHODS["head"] = "HEAD";
})(SUPPORT_METHODS || (SUPPORT_METHODS = {}));
export var SUPPORT_DATATYPES = ['text', 'json', 'blob', 'arrayBuffer', 'formData'];
export var CONTENT_TYPES = {
    'form': 'application/x-www-form-urlencoded; charset=UTF-8',
    'json': 'application/json; charset=utf-8'
};
var DEFAULT_HEADERS = {
    'Accept': 'application/json',
    'Content-Encoding': 'gzip',
    'Content-Type': 'application/json; charset=utf-8'
};
var CORS_CONFIG = {
    'mode': 'cors',
    'credentials': 'include',
    'cache': 'no-cache'
};
// helper
function parseBody(body) {
    if (getType(body) === 'object') {
        return json2string(body, false, true);
    }
    return body;
}
function FetchGen(method) {
    return function (url, params, headers, cors, dataType, originResponse) {
        if (cors === void 0) { cors = CORS_CONFIG; }
        if (dataType === void 0) { dataType = 'json'; }
        if (originResponse === void 0) { originResponse = false; }
        // get restful params
        var restfulParams = !params ? {} : Object.keys(params).filter(function (k) {
            return k.indexOf('@') === 0;
        }).reduce(function (r, k) {
            var _a;
            var _k = k.replace('@', '');
            return Object.assign(r, (_a = {},
                _a[_k] = params[k],
                _a));
        }, {});
        // get send params
        var sendParams = !params ? {} : Object.keys(params).filter(function (k) {
            return k.indexOf('@') !== 0;
        }).reduce(function (r, k) {
            var _a;
            return Object.assign(r, (_a = {},
                _a[k] = params[k],
                _a));
        }, {});
        var _url = compileUrl(url, restfulParams);
        var _headers = Object.assign({}, DEFAULT_HEADERS, headers);
        if (method === SUPPORT_METHODS.get) {
            _url = urlWithQuery(_url, sendParams);
        }
        var _config = __assign({ method: method, headers: _headers }, cors);
        if (method !== SUPPORT_METHODS.get && method !== SUPPORT_METHODS.head) {
            if (_headers['Content-Type'] === CONTENT_TYPES['json']) {
                _config['body'] = JSON.stringify(params);
            }
            else {
                _config['body'] = parseBody(params);
            }
        }
        var _request = new Request(_url, _config);
        return fetch(_request).then(function (res) {
            if (originResponse) {
                return res;
            }
            if (SUPPORT_DATATYPES.indexOf(dataType) >= 0) {
                return res[dataType]();
            }
            return res.json();
        });
    };
}
var Fetch = Object.keys(SUPPORT_METHODS).reduce(function (r, m) {
    var _a;
    return Object.assign(r, (_a = {},
        _a[m] = FetchGen(SUPPORT_METHODS[m]),
        _a));
}, {});
export default Fetch;
