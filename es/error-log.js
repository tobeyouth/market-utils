/**
 * throw new error or use console.error
 */
var silence = process && process.env && process.env.NODE_ENV === 'silence';
export default (function (method, desc, filename) {
    var text = method + " error(" + desc + ").";
    if (filename) {
        text = text + " in this file: " + filename;
    }
    if (silence) {
        console && console.error(text);
    }
    else {
        throw new Error(text);
    }
});
