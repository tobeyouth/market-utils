/**
 * 将 json 格式的数据解析为字符串
 *
 * from:
 *   {
 *     'name': 'a',
 *     'id': 1
 *   }
 *
 * to:
 *   name=a&id=1
 */
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
export default (function (data, encode, empty) {
    if (encode === void 0) { encode = false; }
    if (empty === void 0) { empty = false; }
    var _data = __assign({}, data);
    if (empty) {
        Object.keys(_data).map(function (key) {
            if (_data[key] === undefined || _data[key] === null) {
                delete _data[key];
            }
        });
    }
    var params = Object.keys(_data).map(function (key) {
        if (encode) {
            return key + "=" + encodeURIComponent(_data[key]);
        }
        return key + "=" + _data[key];
    }).join('&');
    return params;
});
