import json2string from './json2string';
/**
 * getQuery('https://market.douban.com/api?page=1&source=market', source)
 * => market
 */
export var getQuery = function (url, name) {
    if (name === void 0) { name = ''; }
    var query = url.indexOf('?') > 0 ? url.split('?').pop() : window.location.search.substring(1);
    if (!query) {
        return null;
    }
    var params = query.split('&');
    var result = {};
    for (var i = 0; i < params.length; i++) {
        var pair = params[i].split('=');
        if (name && pair[0] === name) {
            return pair[1];
        }
        if (pair[0]) {
            result[pair[0]] = pair[1];
        }
    }
    if (name && Object.keys(params).indexOf(name) < 0) {
        return null;
    }
    return result;
};
/**
 * urlWithQuery('http://market.douban.com?page=1', {source: market, id: 1})
 * => 'http://market.douban.com?page=1&source=market&id=1'
 */
export var urlWithQuery = function (url, query, encode) {
    if (encode === void 0) { encode = false; }
    if (!url) {
        return '';
    }
    var originWithPath = url.split('?').shift();
    var oldQuery = getQuery(url);
    var paramsObj = Object.assign({}, oldQuery, query);
    return originWithPath + "?" + json2string(paramsObj, encode);
};
/**
 * compileUrl('/category/:id',{id:277})
 * => '/category/277'
 */
export var compileUrl = function (url, data) {
    var _url = url;
    if (data) {
        Object.keys(data).map(function (key) {
            var re = new RegExp(':' + key, 'gi');
            _url = _url.replace(re, data[key]);
        });
    }
    return _url;
};
