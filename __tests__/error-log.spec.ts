import errorLog from './../src/error-log';

describe('error-log', () => {
  test('throw error', () => {
    const fn = jest.fn(() => {
      errorLog('foo', 'test error', 'foo.js');
    })
    expect(fn).toThrow(Error);
  })
})