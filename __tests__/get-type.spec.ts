import getType from './../src/get-type';

describe('get-type', () => {
  test('array', () => {
    expect(getType([])).toBe('array');
  })
  test('object', () => {
    expect(getType({})).toBe('object');
  })
  test('string', () => {
    expect(getType('')).toBe('string');
  })
  test('number', () => {
    expect(getType(1)).toBe('number');
  })
  test('function', () => {
    expect(getType(() => {})).toBe('function');
  })
  test('map', () => {
    expect(getType(new Map())).toBe('map');
  })
  test('set', () => {
    expect(getType(new Set())).toBe('set');
  })
  test('symbol', () => {
    expect(getType(Symbol())).toBe('symbol');
  })
})