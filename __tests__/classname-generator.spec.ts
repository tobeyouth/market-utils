import clsGenerator from './../src/classname-generator';

describe('classname-generator', () => {
  const cls = clsGenerator('foo');

  test('function', () => {
    expect(typeof(cls)).toBe('function');
  })
  test('creator', () => {
    expect(cls('bar')).toBe('foo-bar');
  })
})