import json2string from './../src/json2string';

describe('json2string', () => {
  test('json2string', () => {
    const json:any = {
      'name': 'foo',
      'index': 1
    };
    expect(json2string(json)).toEqual('name=foo&index=1');
  })
})