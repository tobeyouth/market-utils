import 'jest';
import request from './../src/request';

// mock XMLHttpRequest
function createXHRmock():void {
  let onload:any;
  let onerror:any;

  const open = jest.fn(():void => {});
  // be aware we use *function* because we need to get *this* 
  // from *new XmlHttpRequest()* call
  const send = jest.fn(():void => {}).mockImplementation(function(){
    onload = this.onload.bind(this);
    onerror = this.onerror.bind(this);
  });

  const setRequestHeader = jest.fn(():void => {});
  const abort = jest.fn(():void => {});

  const xhrMockClass = function () {
    return {
      open,
      send,
      abort,
      setRequestHeader
    };
  };

  Object.defineProperty(window, 'XMLHttpRequest', {
    writable: true,
    value: jest.fn().mockImplementation(xhrMockClass)
  });
}

describe('request', () => {
  createXHRmock();
  test('return with abort', () => {
    expect(request({ action: '/' })).toHaveProperty('abort');
  })
  test('abort can be called', () => {
    expect(request({ action: '/' }).abort).toBeInstanceOf(Function)
  })
})