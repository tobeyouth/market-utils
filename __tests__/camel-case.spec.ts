import camelCase from './../src/camel-case';

const str = 'this-is-an-apple';
const str2 = 'this.is.an.apple';

describe('cammel-case', () => {
  test('transform', () => {
    expect(camelCase(str)).toEqual('thisIsAnApple');
  })
  test('transform by dot', () => {
    expect(camelCase(str2, '.')).toEqual('thisIsAnApple');
  })
})
