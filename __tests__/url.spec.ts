import { getQuery, compileUrl, urlWithQuery } from './../src/url';

describe('getQuery', () => {
  const url = 'https://market.douban.com?page=1&page_size=15';
  const query = {
    'page': '1',
    'page_size': '15'
  };
  test('without name', () => {
    expect(getQuery(url)).toEqual(query);
  })
  test('with name', () => {
    expect(getQuery(url, 'page')).toEqual(query.page);
  })
  test('empty', () => {
    expect(getQuery(url, '_page_')).toEqual(null);
  })
  test('no-query', () => {
    expect(getQuery('')).toEqual(null);
  })
})

describe('compileUrl', () => {
  const url = '/api/shop/:id';
  const query = {
    'id': '1'
  };
  test('normal', () => {
    expect(compileUrl(url, query)).toEqual('/api/shop/1');
  })
})

describe('urlWithQuery', () => {
  const url = 'https://market.douban.com';
  const query = {
    'page': '1',
    'page_size': '15'
  };
  test('normal', () => {
    expect(urlWithQuery(url, query)).toEqual(`${url}?page=${query.page}&page_size=${query.page_size}`);
  })
})