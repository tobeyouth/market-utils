import clearQuery from './../src/clear-query';

describe('clear-query', () => {
  const query:any = {
    'foo': 'foo',
    '_undefined': undefined,
    '_NaN': NaN,
    '_null': null,
    '_empty': '',
    '_empty_array': []
  };
  test('clear', () => {
    expect(clearQuery(query)).toEqual({ 'foo': 'foo' });
  })
})