import cookie from './../src/cookie';

describe('cookie', () => {
  Object.defineProperty(window.document, 'cookie', {
    writable: true,
    value: 'myCookie=omnomnom',
  });
  test('set', () => {
    expect(cookie.set({ 'test': 'foo' })).toBe(true);
  });
  test('get', () => {
    expect(cookie.get('test')).toBe('foo')
  });
})