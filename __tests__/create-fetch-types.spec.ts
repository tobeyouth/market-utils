import createFetchTypes from './../src/create-fetch-types';

describe('create-fetch-types', () => {
  const FooFetch = createFetchTypes('foo', 'fetch');

  test('types dict', () => {
    expect(FooFetch).toEqual({
      'fetch': FooFetch.fetch,
      'pending': FooFetch.pending,
      'success': FooFetch.success,
      'fail': FooFetch.fail,
    })
  })

  test('types value', () => {
    expect(FooFetch.fetch).toBe('FOO.FETCH');
    expect(FooFetch.success).toBe('FOO.FETCH.SUCCESS');
    expect(FooFetch.fail).toBe('FOO.FETCH.FAIL');
    expect(FooFetch.pending).toBe('FOO.FETCH.PENDING');
  })
})