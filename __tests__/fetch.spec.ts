import Fetch from './../src/fetch';

describe('fetch', () => {
  test('return promise', () => {
    expect(Fetch.get('/test')).toBeInstanceOf(Promise);
  })
})