# market-utils

一些常用方法和常量的集合，提供了 `esmodule`和`types`导入，建议使用这两种方式导入。

已包含的方法有：

#### async-status

异步状态常量

```
import { DEFAULT, PENDING, SUCCESS, FAIL } from 'market-utils/src/async-status'
```

#### camel-case

将中划线命名，转换成驼峰命名

```
import camelCase from 'market-utils/src/camel-case';
camelCase('this-is-an-apple');

// return: thisIsAnApple
```

#### classname-generator

根据自定义的 scope 和 prefix,构建一个 classname 生成器 

```
import clsGenerator from 'market-utils/src/classname-generator';

const clsCreator = clsGenerator('foo', 'bar');
let cls = clsCreator('content');

// cls => 'bar-foo-content'
```


#### clear-query

清除 `query` 中的`空字符串`, `undefined`, `null` 和 `NaN`以及`空数组`

```
import clearQuery from 'market-utils/src/clear-query';

const query = {
  'foo': 'foo',
  '_undefined': undefined,
  '_NaN': NaN,
  '_null': null,
  '_empty': '',
  '_empty_array': []
}
const _query = clearQuery(query);

// _query => { 'foo': foo }
```

#### cookie

提供常用的 cookie 操作，`get`和`set`

```
import cookie from 'market-utils/src/cookie';

cookie.set({'foo': 'FOO'}, days, domain, path);
// return true/false

cookie.get('foo')
// reeturn: 'FOO'
```


#### create-fetch-types

提供多个异步请求的状态组合，形如：

```
{
  'create': `${target}.CREATE`,
  'pending': `${target}.PENDING`,
  'success': `${target}.SUCCESS`,
  'fail': `${target}.FAIL`,
}
```

```
import createFetchTypes from 'market-utils/src/create-fetch-types';
const articleCreate = createFetchTypes('article', 'create');
// articleCreate => {
  'create': `ARTICLE.CREATE`,
  'pending': `ARTICLE.PENDING`,
  'success': `ARTICLE.SUCCESS`,
  'fail': `ARTICLE.FAIL`,
}
```


#### error-log

一个错误输出方法，默认会抛出 `Error`，将 `process.env.NODE_ENV`设置为`silence`，会改为使用 `console.error`输出错误

```
import errorLog from 'market-utils/src/error-log';
errorLog(method, desc, filename)
// throw Error(`${method} error(${desc}) in ${filename}`);
```


#### fetch

一个简化版的 fetch 生成器, 提供了以下方法
- get
- post
- put
- delete
- patch
- head
- options

这些方法会返回对应 `method`的 `window.fetch` 请求

参数如下：

- url: 请求 url，可以通过 `:` 生成 restful 格式的 url，例如`/article/:id`
- params: 请求携带的参数，在参数前加上 `@`，可以声明该字段用于 restful api； 例如:`{title: '测试标题', id: 1}`, 将会向 `/article/1/` 发送参数为 `{title: '测试标题'}`的请求
- headers： 自定义 headers
- cors: 跨越设置
- dataType: response 解析格式, 目前支持: `text`, `json`, `blob`, `arrayBuffer`, `formData`
- originResponse: 是否不使用 `dataType` 设置的解析格式处理 response，直接返回


```
import Fetch from 'market-utils/src/fetch';
Fetch.get(API, query).then(res => {}).catch((err) => {})
```

#### get-type
- 
获取变量的类型（转换为小写格式）

```
import getType from 'market-utils/src/get-type';

getType([]);
// return 'array'
getType({})
// return 'object'
```

#### json2string

将 json 格式的对象转换为`key=val`的形式

```
import json2string from 'market-utils/src/json2string';
json2string({'foo': 'FOO'})

// return: 'foo=FOO'
```

#### range
类似 python 中的 `range`方法，返回数组

```
import range from 'market-utils/src/range';
range(1, 5);
// return [1,2,3,4,5]
```

### request

使用 XHMLHTTPRequest 的异步请求方法，传入 `config` 参数设置。`config`可设置项如下：

- action: 请求发送地址
- headers: 请求 headers
- data: 请求携带的数据
- file: 请求携带的 file
- filename: 请求携带 file 的 name
- withCredentials: 跨越是否携带 cookie
- proxy: 请求前处理 action，并返回新的 action
- onProgress: progress 回调
- onSuccess: success 回调
- onError: error 回调


#### url

一些处理 url 相关的方法：

- getQuery: 从传入url 的 query 中取值
- urlWithQuery: 把传入的 query 转成 url.query，并加到 url 后面
- compileUrl: 将 url 和传入的 data，转化成 restful api 的形式

