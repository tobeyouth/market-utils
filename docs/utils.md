---
title: utils
---

### utils

常用方法

##### API


|    方法名        | 说明 |参数及方法调用 | 
| --------------- | --- | --- | --- | ---|
| CamelCase       | 字符串首字母大写  |  `CamelCase('funcName') => 'FuncName'`|
| CreateContainer | 创建 dom 元素插入 `body` 中并返回| `CreateContainer(tag='div', root=document.body)`|
| GetContainer    | 获取某个 `dom` 元素并返回 |`getContainer(container, defaultContainer=doc.body)`|
| ErrorLog        | 错误输出 | `ErrorLog(method, desc, filename)`
| Json2string     | 将 json 格式的数据解析为字符串| `Json2string({name: 'a', index: 1}) => 'name=a&index=1'`|
| UrlParse        | 解析 `url` 及 `query` 集成方法|`getQuer, proxy2market, urlWithQuery, compileUrl`|
| Range           | 生成指定范围的数组 |`Range(2, 8) => [2,3,4,5,6,7,8]`|
| Fetch           | 发送 `API` 请求 get, post, put, delete |
| AsyncStatus     | 异步常量 | `DEFAULT, PENDING, SUCCESS, FAIL`|



