declare const _default: (scope: string, prefix?: string) => Function;
export default _default;
