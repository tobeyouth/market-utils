/**
 * cls generator
 */
const PREFIX = '';
let EXIST_CLASSNAME:Array<string> = [];

export default (scope:string, prefix:string=PREFIX):Function => {
  return (name:string):string => {
    let classname = [prefix, scope, name].filter((item) => !!item).join('-');
    if (EXIST_CLASSNAME.indexOf(classname) > -1) {
      console.warn(`${classname} 已经存在, 换个 classname 吧`);
    }
    return classname;
  }
}

