export const DEFAULT = 'default';
export const PENDING = 'pending';
export const FAIL = 'fail';
export const SUCCESS = 'success';

export const ASYNC_STATUS = [DEFAULT, PENDING, FAIL, SUCCESS];