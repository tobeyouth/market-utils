export interface Proxy {
    (action: string): string;
}
export interface OnProgress {
    (e: Event): void;
}
export interface OnSuccess {
    (res: any, xhr?: XMLHttpRequest): void;
}
export interface OnError {
    (err: any, option?: any): void;
}
export interface Option {
    action: string;
    headers?: any;
    data?: any;
    file?: File;
    filename?: string;
    withCredentials?: boolean;
    proxy?: Proxy;
    onProgress?: OnProgress;
    onSuccess?: OnSuccess;
    onError?: OnError;
}
export interface Abort {
    (xhr: XMLHttpRequest): void;
}
export interface RequestResult {
    abort: Abort;
}
export interface Progress extends ProgressEvent {
    percent?: number;
}
declare const _default: (option: Option) => RequestResult;
export default _default;
