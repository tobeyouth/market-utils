/**
 * return param type
 */
export default (param:any):string => {
  const reg = /^\[object ([a-z]+)\]$/gi;
  const r:string = Object.prototype.toString.call(param)
    .replace((reg), ($1:string, $2:string):string => $2);
  if (r) {
    return r.toLocaleLowerCase();
  }
  return '';
}