/**
 * 创建带 scope 和 status 的 fetch action types
 * example:
 * createFetchTypes('topics', 'fetch')
 * return:
 * {
 *  fetch: 'TOPICS.FETCH',
 *  success: 'TOPICS.FETCH.SUCCESS',
 *  fail: 'TOPICS.FETCH.FAIL'
 * }
 */
import { SUCCESS, FAIL, PENDING } from './async-status';

export interface FetchTypes {
  readonly [actionName: string]: any,
  readonly pending: string,
  readonly success: string,
  readonly fail: string
}

export default (scope: string, name: string): FetchTypes => {
  if (!scope) {
    throw new Error('定义 action types 时缺少 scope');
  }

  const _default = [scope, name].map((item) => item.toUpperCase()).join('.');
  const _success = [scope, name, SUCCESS].map((item) => item.toUpperCase()).join('.');
  const _pending = [scope, name, PENDING].map((item) => item.toUpperCase()).join('.');
  const _fail = [scope, name, FAIL].map((item) => item.toUpperCase()).join('.');

  return {
    [name]: _default,
    pending: _pending,
    success: _success,
    fail: _fail
  }
}
