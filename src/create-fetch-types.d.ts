export interface FetchTypes {
    readonly [actionName: string]: any;
    readonly pending: string;
    readonly success: string;
    readonly fail: string;
}
declare const _default_1: (scope: string, name: string) => FetchTypes;
export default _default_1;
