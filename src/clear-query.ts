/**
 * 清除 query 中为空或者未定义的 param
 */

// helper
function isCanClear(val:any): boolean {
  return val === '' || val === undefined || 
         val === null || (typeof(val) === 'number' && isNaN(val)) ||
         (Array.isArray(val) && val.length === 0)
}

export default (query: any): object => {
  let _query = { ...query }
  for (let k in _query) {
    if (_query.hasOwnProperty(k) && isCanClear(_query[k])) {
      delete _query[k]
    }
  }
  return _query
}