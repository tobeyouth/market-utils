/**
 * normal request
 * if you want proimse style, please use fetch.js
 */

export interface Proxy {
  (action:string):string
}

export interface OnProgress {
  (e:Event):void
}

export interface OnSuccess {
  (res:any, xhr?:XMLHttpRequest):void
}

export interface OnError {
  (err:any, option?:any):void
}

export interface Option {
  action: string,
  headers?: any,
  data?: any,
  file?: File,
  filename?: string,
  withCredentials?: boolean,
  proxy?: Proxy,
  onProgress?: OnProgress,
  onSuccess?: OnSuccess,
  onError?: OnError
}

export interface Abort {
  (xhr:XMLHttpRequest):void
}

export interface RequestResult {
  abort:Abort
}

export interface Progress extends ProgressEvent {
  percent?: number
}

// helpers
function getError(option:any, xhr:any):object {
  const msg:string = `cannot post ${option.action} ${xhr.status}'`;
  let err:any = new Error(msg);
  err.status = xhr.status;
  err.method = 'post';
  err.url = option.action;
  return err;
}

function getBody(xhr:XMLHttpRequest):string | object | Array<any> {
  const text = xhr.responseText || xhr.response;
  if (!text) {
    return text;
  }

  try {
    return JSON.parse(text);
  } catch (e) {
    return text;
  }
}

export default (option:Option):RequestResult => {
  const xhr:XMLHttpRequest = new XMLHttpRequest();

  if (option.onProgress && xhr.upload) {
    xhr.upload.onprogress = function progress(e:Progress):void {
      if (e.total > 0) {
        e.percent = e.loaded / e.total * 100;
      }
      option.onProgress(e);
    };
  }

  const formData = new FormData();

  if (option.data) {
    Object.keys(option.data).map((key) => {
      formData.append(key, option.data[key]);
    });
  }

  formData.append(option.filename || 'file', option.file);

  xhr.onerror = function error(e) {
    option.onError(e);
  };

  xhr.onload = function onload() {
    if (xhr.status < 200 || xhr.status >= 300) {
      return option.onError(getError(option, xhr), getBody(xhr));
    }

    option.onSuccess(getBody(xhr), xhr);
  };

  let _action = option.proxy ? option.proxy(option.action) : option.action
  xhr.open('post', _action, true);

  if (option.withCredentials && 'withCredentials' in xhr) {
    xhr.withCredentials = true;
  }

  const headers = option.headers || {};

  if (headers['X-Requested-With'] !== null) {
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  }

  for (const h in headers) {
    if (headers.hasOwnProperty(h) && headers[h] !== null) {
      xhr.setRequestHeader(h, headers[h]);
    }
  }
  xhr.send(formData);

  return {
    abort() {
      xhr.abort();
    },
  };
}
