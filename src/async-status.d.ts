export declare const DEFAULT = "default";
export declare const PENDING = "pending";
export declare const FAIL = "fail";
export declare const SUCCESS = "success";
export declare const ASYNC_STATUS: string[];
