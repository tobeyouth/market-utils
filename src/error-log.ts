/**
 * throw new error or use console.error
 */
const silence:boolean = process && process.env && process.env.NODE_ENV === 'silence';

export default (method:string, desc:string, filename:string):void => {
  let text = `${method} error(${desc}).`
  if (filename) {
    text = `${text} in this file: ${filename}`;
  }
  if (silence) {
    console && console.error(text);
  } else {
    throw new Error(text);
  }
}