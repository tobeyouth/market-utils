declare const _default: (data: any, encode?: boolean, empty?: boolean) => string;
export default _default;
