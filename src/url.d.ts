export interface GetQuery {
    (url: string, name?: string): any;
}
export interface UrlWithQuery {
    (url: string, query: any): string;
}
export interface CompileUrl {
    (url: string, data: any): string;
}
export declare const getQuery: GetQuery;
export declare const urlWithQuery: UrlWithQuery;
export declare const compileUrl: CompileUrl;
