import json2string from './json2string';

export interface GetQuery {
  (url: string, name?: string): any;
}

export interface UrlWithQuery {
  (url: string, query: any): string;
}

export interface CompileUrl {
  (url: string, data: any): string;
}


/**
 * getQuery('https://market.douban.com/api?page=1&source=market', source) 
 * => market
 */
export const getQuery: GetQuery = (url:string, name:string = ''): null | string => {
  let query:string = url.indexOf('?') > 0 ? url.split('?').pop() : window.location.search.substring(1);
  if (!query) {
    return null;
  }

  let params:Array<string> = query.split('&');
  let result:any = {};
  
  for (let i = 0; i < params.length; i++) {
    let pair:any = params[i].split('=');

    if (name && pair[0] === name) {
      return pair[1];
    }

    if (pair[0]) {
      result[pair[0]] = pair[1];
    }
  }

  if (name && Object.keys(params).indexOf(name) < 0) {
    return null;
  }

  return result;
}


/**
 * urlWithQuery('http://market.douban.com?page=1', {source: market, id: 1})
 * => 'http://market.douban.com?page=1&source=market&id=1'
 */
export const urlWithQuery: UrlWithQuery = (url:string, query:any, encode: boolean = false): string => {
  if (!url) {
    return '';
  }
  let originWithPath = url.split('?').shift();
  let oldQuery = getQuery(url);
  let paramsObj = Object.assign({}, oldQuery, query);
  return `${originWithPath}?${json2string(paramsObj, encode)}`;
}

/**
 * compileUrl('/category/:id',{id:277})
 * => '/category/277'
 */
export const compileUrl: CompileUrl = (url:string, data:any): string => {
  let _url = url;
  if (data) {
    Object.keys(data).map((key) => {
      let re = new RegExp(':' + key, 'gi');
      _url = _url.replace(re, data[key]);
    })
  }

  return _url;
}
