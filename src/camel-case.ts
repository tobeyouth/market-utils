export default (name:string, separator = '-'):string => {
  const firstPattern = /^\S/gi;
  const camelPattern = new RegExp(`\\${separator}([a-z]{1})`, 'gi');

  return name.replace(firstPattern, function ($1) {
    return $1.toLowerCase();
  }).replace(camelPattern, function ($1, $2) {
    return $2.toUpperCase();
  })
}