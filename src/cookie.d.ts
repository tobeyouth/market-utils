declare const _default: {
    set: (dict: any, days?: number, domain?: string, path?: string) => boolean;
    get: (name: string) => string;
};
export default _default;
