import 'whatwg-fetch';
export declare enum SUPPORT_METHODS {
    get = "GET",
    post = "POST",
    put = "PUT",
    delete = "DELETE",
    options = "OPTIONS",
    patch = "PATCH",
    head = "HEAD"
}
export declare const SUPPORT_DATATYPES: string[];
export declare const CONTENT_TYPES: {
    'form': string;
    'json': string;
};
declare const Fetch: any;
export default Fetch;
