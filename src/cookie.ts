const DEFALT_DOMAIN = 'douban.com';
const DEFALT_PATH = '/';

export default {
  set: (dict: any, days: number = 1, 
    domain: string = DEFALT_DOMAIN, path: string = DEFALT_PATH): boolean => {
    
    let date = new Date();
    let expires;

    date.setTime(date.getTime() + ((days || 30) * 24 * 60 * 60 * 1000));
    expires = '; expires=' + date.toUTCString();

    try {
      for (let k in dict) {
        if (dict.hasOwnProperty(k)) {
          document.cookie = `${k}=${dict[k]}${expires};domain=${domain};path=${path}`;
        }
      }
    } catch {
      return false
    }

    return true
  },
  get: (name:string):string => {
    let n = `${name}=`;
    let ca = document.cookie.split(';');
    let i;
    let c;

    for (i = 0; i < ca.length; i++) {
      c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(n) === 0) {
        return c.substring(n.length, c.length).replace(/\"/g, '');
      }
    }
    return '';
  }
}