import 'whatwg-fetch';
import json2string from './json2string';
import { urlWithQuery, compileUrl } from './url';
import getType from './get-type';

export enum SUPPORT_METHODS {
  get = 'GET',
  post = 'POST',
  put = 'PUT',
  delete = 'DELETE',
  options = 'OPTIONS',
  patch = 'PATCH',
  head = 'HEAD'
}

export const SUPPORT_DATATYPES = ['text', 'json', 'blob', 'arrayBuffer', 'formData']

export const CONTENT_TYPES = {
  'form': 'application/x-www-form-urlencoded; charset=UTF-8',
  'json': 'application/json; charset=utf-8'
}

interface headersInterface {
  [propName:string]: string
}
interface paramsInterface {
  [propName:string]: any
}
interface requestInetface {
  mode: string,
  credentials: string,
  cache: string
}

const DEFAULT_HEADERS = {
  'Accept': 'application/json',
  'Content-Encoding': 'gzip',
  'Content-Type': 'application/json; charset=utf-8'
  
}

const CORS_CONFIG = {
  'mode': 'cors',
  'credentials': 'include',
  'cache': 'no-cache'
}

// helper
function parseBody(body:any): any {
  if (getType(body) === 'object') {
    return json2string(body, false, true)
  }
  return body
}

function FetchGen(method:SUPPORT_METHODS): Function {

  return function(url:string, params?:paramsInterface, headers?:any, 
    cors:any = CORS_CONFIG, dataType:string = 'json', originResponse:boolean = false) {
    
    // get restful params
    const restfulParams:any = !params ? {} : Object.keys(params).filter((k) => {
      return k.indexOf('@') === 0;
    }).reduce((r, k) => {
      const _k = k.replace('@', '')
      return Object.assign(r, {
        [_k]: params[k]
      })
    }, {})
    // get send params
    const sendParams:any = !params ? {} : Object.keys(params).filter((k) => {
      return k.indexOf('@') !== 0;
    }).reduce((r, k) => {
      return Object.assign(r, {
        [k]: params[k]
      })
    }, {})

    let _url = compileUrl(url, restfulParams);
    let _headers:headersInterface = Object.assign({}, DEFAULT_HEADERS, headers);
    
    if (method === SUPPORT_METHODS.get) {
      _url = urlWithQuery(_url, sendParams);
    }

    let _config = {
      method,
      headers: _headers,
      ...cors
    }

    if (method !== SUPPORT_METHODS.get && method !== SUPPORT_METHODS.head) {
      if (_headers['Content-Type'] === CONTENT_TYPES['json']) {
        _config['body'] = JSON.stringify(params);
      } else {
        _config['body'] = parseBody(params);
      }
    }

    let _request = new Request(_url, _config);

    return fetch(_request).then((res:any) => {
      if (originResponse) {
        return res;
      }
      if (SUPPORT_DATATYPES.indexOf(dataType) >= 0) {
        return res[dataType]();
      }
      return res.json();
    })
  }
}

const Fetch = Object.keys(SUPPORT_METHODS).reduce((r:any, m:string):any => {
  return Object.assign(r, {
    [m]: FetchGen(SUPPORT_METHODS[m as keyof typeof SUPPORT_METHODS])
  })
}, {});

export default Fetch;