(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = (function (param) {
        var reg = /^\[object ([a-z]+)\]$/gi;
        var r = Object.prototype.toString.call(param)
            .replace((reg), function ($1, $2) { return $2; });
        if (r) {
            return r.toLocaleLowerCase();
        }
        return '';
    });
});
//# sourceMappingURL=get-type.js.map