(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = (function (name, separator) {
        if (separator === void 0) { separator = '-'; }
        var firstPattern = /^\S/gi;
        var camelPattern = new RegExp("\\" + separator + "([a-z]{1})", 'gi');
        return name.replace(firstPattern, function ($1) {
            return $1.toLowerCase();
        }).replace(camelPattern, function ($1, $2) {
            return $2.toUpperCase();
        });
    });
});
//# sourceMappingURL=camel-case.js.map