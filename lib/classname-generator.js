(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var PREFIX = '';
    var EXIST_CLASSNAME = [];
    exports.default = (function (scope, prefix) {
        if (prefix === void 0) { prefix = PREFIX; }
        return function (name) {
            var classname = [prefix, scope, name].filter(function (item) { return !!item; }).join('-');
            if (EXIST_CLASSNAME.indexOf(classname) > -1) {
                console.warn(classname + " \u5DF2\u7ECF\u5B58\u5728, \u6362\u4E2A classname \u5427");
            }
            return classname;
        };
    });
});
//# sourceMappingURL=classname-generator.js.map