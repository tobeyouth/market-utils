var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./json2string"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var json2string_1 = __importDefault(require("./json2string"));
    exports.getQuery = function (url, name) {
        if (name === void 0) { name = ''; }
        var query = url.indexOf('?') > 0 ? url.split('?').pop() : window.location.search.substring(1);
        if (!query) {
            return null;
        }
        var params = query.split('&');
        var result = {};
        for (var i = 0; i < params.length; i++) {
            var pair = params[i].split('=');
            if (name && pair[0] === name) {
                return pair[1];
            }
            if (pair[0]) {
                result[pair[0]] = pair[1];
            }
        }
        if (name && Object.keys(params).indexOf(name) < 0) {
            return null;
        }
        return result;
    };
    exports.urlWithQuery = function (url, query, encode) {
        if (encode === void 0) { encode = false; }
        if (!url) {
            return '';
        }
        var originWithPath = url.split('?').shift();
        var oldQuery = exports.getQuery(url);
        var paramsObj = Object.assign({}, oldQuery, query);
        return originWithPath + "?" + json2string_1.default(paramsObj, encode);
    };
    exports.compileUrl = function (url, data) {
        var _url = url;
        if (data) {
            Object.keys(data).map(function (key) {
                var re = new RegExp(':' + key, 'gi');
                _url = _url.replace(re, data[key]);
            });
        }
        return _url;
    };
});
//# sourceMappingURL=url.js.map