(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./async-status"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var async_status_1 = require("./async-status");
    exports.default = (function (scope, name) {
        var _a;
        if (!scope) {
            throw new Error('定义 action types 时缺少 scope');
        }
        var _default = [scope, name].map(function (item) { return item.toUpperCase(); }).join('.');
        var _success = [scope, name, async_status_1.SUCCESS].map(function (item) { return item.toUpperCase(); }).join('.');
        var _pending = [scope, name, async_status_1.PENDING].map(function (item) { return item.toUpperCase(); }).join('.');
        var _fail = [scope, name, async_status_1.FAIL].map(function (item) { return item.toUpperCase(); }).join('.');
        return _a = {},
            _a[name] = _default,
            _a.pending = _pending,
            _a.success = _success,
            _a.fail = _fail,
            _a;
    });
});
//# sourceMappingURL=create-fetch-types.js.map