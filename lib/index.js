(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports", "./async-status", "./url", "./camel-case", "./clear-query", "./cookie", "./create-fetch-types", "./json2string", "./range", "./request", "./classname-generator", "./get-type", "./fetch", "./error-log"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var async_status_1 = require("./async-status");
    exports.DEFAULT = async_status_1.DEFAULT;
    exports.PENDING = async_status_1.PENDING;
    exports.FAIL = async_status_1.FAIL;
    exports.SUCCESS = async_status_1.SUCCESS;
    var url_1 = require("./url");
    exports.getQuery = url_1.getQuery;
    exports.compileUrl = url_1.compileUrl;
    exports.urlWithQuery = url_1.urlWithQuery;
    var camel_case_1 = require("./camel-case");
    exports.camelCase = camel_case_1.default;
    var clear_query_1 = require("./clear-query");
    exports.clearQuery = clear_query_1.default;
    var cookie_1 = require("./cookie");
    exports.cookie = cookie_1.default;
    var create_fetch_types_1 = require("./create-fetch-types");
    exports.createFetchTypes = create_fetch_types_1.default;
    var json2string_1 = require("./json2string");
    exports.json2string = json2string_1.default;
    var range_1 = require("./range");
    exports.range = range_1.default;
    var request_1 = require("./request");
    exports.request = request_1.default;
    var classname_generator_1 = require("./classname-generator");
    exports.clsGenerator = classname_generator_1.default;
    var get_type_1 = require("./get-type");
    exports.getType = get_type_1.default;
    var fetch_1 = require("./fetch");
    exports.Fetch = fetch_1.default;
    exports.SUPPORT_DATATYPES = fetch_1.SUPPORT_DATATYPES;
    exports.SUPPORT_METHODS = fetch_1.SUPPORT_METHODS;
    var error_log_1 = require("./error-log");
    exports.errorLog = error_log_1.default;
});
//# sourceMappingURL=index.js.map