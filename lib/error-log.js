(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var silence = process && process.env && process.env.NODE_ENV === 'silence';
    exports.default = (function (method, desc, filename) {
        var text = method + " error(" + desc + ").";
        if (filename) {
            text = text + " in this file: " + filename;
        }
        if (silence) {
            console && console.error(text);
        }
        else {
            throw new Error(text);
        }
    });
});
//# sourceMappingURL=error-log.js.map