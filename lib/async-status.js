(function (factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        var v = factory(require, exports);
        if (v !== undefined) module.exports = v;
    }
    else if (typeof define === "function" && define.amd) {
        define(["require", "exports"], factory);
    }
})(function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.DEFAULT = 'default';
    exports.PENDING = 'pending';
    exports.FAIL = 'fail';
    exports.SUCCESS = 'success';
    exports.ASYNC_STATUS = [exports.DEFAULT, exports.PENDING, exports.FAIL, exports.SUCCESS];
});
//# sourceMappingURL=async-status.js.map